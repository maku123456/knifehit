﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class View<T> : View where T : View<T>
{
    public virtual void Awake()
    {

    }

    public virtual void Start()
    {

    }
    public virtual void OnEnable()
    {

    }

    public virtual void OnDisable()
    {

    }

   
    public void GotToView<T>() where T : View
    {
        var view = ViewManager.Instance.GotToView<T>();
        view.Open();
    }

    public override void OnDeviceBackButtonPressed()
    {

    }

    public override void Open()
    {
        this.gameObject.SetActive(true);
    }

    public override void Close()
    {
        this.gameObject.SetActive(false);
    }
}

public abstract class View : MonoBehaviour
{
    public abstract void Open();

    public abstract void Close();
    public abstract void OnDeviceBackButtonPressed();
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ViewManager : MonoBehaviour
{
    private static ViewManager instance;

    public static ViewManager Instance
    {
        get
        {
            if (instance == null)
            {
                var go = FindObjectOfType<ViewManager>();
                instance = go.GetComponent<ViewManager>();
                DontDestroyOnLoad(go);
            }
            return instance;
        }
    }

    [SerializeField] private List<View> viewList = new List<View>();

    internal T GotToView<T>() where T : View
    {
        return (T)viewList.Find(t => t.GetType().Name == typeof(T).Name);
    }

    internal void AddView(View view)
    {
        viewList.Add(view);
    }
}

﻿using Core.Concrete;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Board
{
    public class Board : MonoBehaviour, IBoard
    {
        public IStage CurrentStage { get; set; }
        public IBoardRotater BoardRotater { get; set; }

        private  void Start()
        {
            BoardRotater = FindObjectOfType<BoardRotator>();
        }
        public void InitializeStage(IStage stage)
        {
            BoardRotater.CurrentStage = stage;
            BoardRotater.ResetBoardRoattion = true;
            //TODO: IBoard rotor should take the value from here 
        }
    }
}

﻿using Core.Concrete;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Board
{
    [RequireComponent(typeof(WheelJoint2D))]
    public class BoardRotator : MonoBehaviour, IBoardRotater
    {
        private WheelJoint2D wheelJoint;
        private JointMotor2D motor;

        public IStage CurrentStage { get; set; }
        public bool ResetBoardRoattion { get; set; }

        private void Start()
        {
            wheelJoint = GetComponent<WheelJoint2D>();
            motor = new JointMotor2D();
            StartCoroutine("PlayRotationPattern");
        }

        private IEnumerator PlayRotationPattern()
        {
            int rotationIndex = 0;

            while (true)
            {
                yield return new WaitForFixedUpdate();
                if (ResetBoardRoattion)
                {

                    Rotate(rotationIndex);

                    yield return new WaitForSecondsRealtime(CurrentStage.RotationModel[rotationIndex].Duration);
                    rotationIndex++;
                    rotationIndex = rotationIndex < CurrentStage.RotationModel.Count ? rotationIndex : 0;
                }
            }
        }

        public void Rotate(int rotationIndex)
        {
            motor.motorSpeed = CurrentStage.RotationModel[rotationIndex].Speed;
            motor.maxMotorTorque = 10000;
            wheelJoint.motor = motor;
        }
    }
}

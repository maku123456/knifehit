﻿using Core.Views;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay
{
    public enum GameState
    {
        Start,
        Wait,
        End,
    }


    public sealed class GameManager : MonoBehaviour
    {
        private void OnEnable()
        {
            EventDispatcher.OnGameEndedEvent += OnGameEnded;
        }

        private void OnDisable()
        {
            EventDispatcher.OnGameEndedEvent -= OnGameEnded;
        }

        private void OnGameEnded()
        {
            var view = ViewManager.Instance.GotToView<RetryView>();
            ScreenFader._instance.FadeScreen(FadeColor.Black, FadeType.FadeOut);

            view.Open();
        }


    }
}
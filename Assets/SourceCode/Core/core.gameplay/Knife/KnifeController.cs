﻿using Core.Concrete;
using Core.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Knife
{
    public class KnifeController : MonoBehaviour, IKnifeController
    {
        [SerializeField] private GameObject knifeObject;
        [SerializeField] private Vector2 spawnPosition;

        public IKnifeUnit TotalKnifeCount { get; set; }
        public IKnifeUnit KnifeCount { get; set; }
        public GameObject KnifeObject => knifeObject;
        public Vector2 SpawnPosition => spawnPosition;
        public IStage CurrentStage { get; set; }
        public List<IKnife> KnifeStorage { get; set; }
        public IKnife CurrentKnife { get; set; }

        IObjectPool knifePool;

        SharedView sharedView;

        protected bool isReady = true;
        protected int currentKnife = 0;
        private void Awake()
        {
            knifePool = new ObjectPool(KnifeObject, 5, this.transform);
            KnifeStorage = new List<IKnife>();
            sharedView = ViewManager.Instance.GotToView<SharedView>();

        }

        private void OnEnable()
        {
            EventDispatcher.OnGameStartedEvent += StartStage;
            EventDispatcher.onBoardHitEvent += OnSuccesfullKnifeHit;
            EventDispatcher.OnKnifeHitEvent += OnUnSuccesfulKnifeHit;
            EventDispatcher.onKnifeInputActivityEvent += ThrowKnifeHandler;
        }

        private void OnDisable()
        {
            EventDispatcher.OnGameStartedEvent += StartStage;

            EventDispatcher.onBoardHitEvent -= OnSuccesfullKnifeHit;
            EventDispatcher.OnKnifeHitEvent -= OnUnSuccesfulKnifeHit;
            EventDispatcher.onKnifeInputActivityEvent -= ThrowKnifeHandler;
        }

        private void StartStage()
        {
            isReady = true;
        }

        private void ThrowKnifeHandler()
        {
            CurrentKnife.ThrowKnife();
        }

        public void OnSuccesfullKnifeHit(Transform target)
        {
            if (!isReady)
                return;

            currentKnife--;
            if (currentKnife <= 0)
            {
                isReady = false;
                EventDispatcher.DispatchOnLoadNextStage();
                ResetKnifes();
                return;
            }

            sharedView.DeductKnifes();
            SpawnKnife();
        }

        public void OnUnSuccesfulKnifeHit(Transform target)
        {
            StartCoroutine(DelayCall());
        }

        IEnumerator DelayCall()
        {
            yield return new WaitForSeconds(1f);
            EventDispatcher.DispatchOnGameEnded();
        }

        public void SpawnKnife()
        {
            if (!isReady)
                return;
            var go = knifePool.GetObject(false);
            go.transform.position = SpawnPosition;
            go.transform.rotation = Quaternion.identity;
            go.SetActive(true);
            var component = go.GetComponent<IKnife>();
            CurrentKnife = component;
            CurrentKnife.InitKnife();
            KnifeStorage.Add(component);
        }

        private void StartGamePlay()
        {
            ResetKnifes();
        }


        private void ResetKnifes()
        {
            KnifeStorage.Clear();
            StartCoroutine(ReturnKnife());
        }

        IEnumerator ReturnKnife()
        {
            knifePool.ReturnAllToPool();
            var knifes = knifePool.GetAllObjects();
            for (int i = 0; i < knifes.Count; i++)
            {
                var component = knifes[i].GetComponent<IKnife>();
                component.InitKnife();
            }
            yield return new WaitForSeconds(0.2f);
            SpawnKnife();
            isReady = true;
        }

        public void SetUpKnifeStage(IStage stage)
        {
            EventDispatcher.DisptachNextStage(stage.StageUnit.StageId);
            StartGamePlay();
            CurrentStage = stage;
            TotalKnifeCount = stage.StageUnit;
            KnifeCount = stage.StageUnit;
            currentKnife = KnifeCount.Count;
            sharedView.SetKnifes(TotalKnifeCount.Count);
        }
    }
}
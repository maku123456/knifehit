﻿using Core.Concrete;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Knife
{
    public class Knife : MonoBehaviour, IKnife
    {
        [SerializeField] private Vector2 throwForce;
        private Rigidbody2D rb;
        private BoxCollider2D knifeCollider;
        KnifeController KnifeController;
        public Vector2 ThrowForce => throwForce;
        public bool IsKnifeUseable { get; set; }

        private void Awake()
        {
            IsKnifeUseable = true;
            rb = GetComponent<Rigidbody2D>();
            knifeCollider = GetComponent<BoxCollider2D>();
            KnifeController = FindObjectOfType<KnifeController>();
        }

        public void ThrowKnife()
        {
            rb.AddForce(throwForce, ForceMode2D.Impulse);
            rb.gravityScale = 1;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (!IsKnifeUseable)
                return;

            IsKnifeUseable = false;

            if (collision.collider.tag == "Board")
            {
                OnBoardCollision(collision);
            }
            else if (collision.collider.tag == "Knife")
            {
                OnKnifeCollision(collision);
            }
        }

        private void OnKnifeCollision(Collision2D collision)
        {
            rb.velocity = new Vector2(rb.velocity.x, -2);
            EventDispatcher.DisptachOnKnifeHit(collision.transform);
        }

        private void OnBoardCollision(Collision2D collision)
        {
            rb.velocity = new Vector2(0, 0);
            rb.bodyType = RigidbodyType2D.Kinematic;
            transform.SetParent(collision.collider.transform);

            knifeCollider.offset = new Vector2(knifeCollider.offset.x, -0.75f);
            knifeCollider.size = new Vector2(knifeCollider.size.x, 1.2f);
            EventDispatcher.DisptachOnBoardHit(collision.transform);
        }

        public void InitKnife()
        {
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.gravityScale = 0;
            knifeCollider.offset = new Vector2(knifeCollider.offset.x, -0.4f);
            knifeCollider.size = new Vector2(knifeCollider.size.x, 1f);
            this.transform.SetParent(KnifeController.transform);
            IsKnifeUseable = true;
        }
    }
}

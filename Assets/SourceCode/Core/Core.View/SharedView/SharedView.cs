﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using TMPro;

namespace Core.Views
{
    public class SharedView : View<SharedView>
    {
        [SerializeField] private EventTrigger inputActivity;
        [SerializeField] private Settings settings;

        public override void OnEnable()
        {
            EventDispatcher.onStageLoadEvent += UpdateStage;
            EventDispatcher.onScoreUpdateEvent += UpdateScore;

            base.OnEnable();
        }

        public override void OnDisable()
        {
            EventDispatcher.onStageLoadEvent -= UpdateStage;
            EventDispatcher.onScoreUpdateEvent -= UpdateScore;
            base.OnDisable();
        }

        public override void Start()
        {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((data) => { OnInputActivity((PointerEventData)data); });
            inputActivity.triggers.Add(entry);
        }

        private void OnInputActivity(PointerEventData data)
        {
            EventDispatcher.DisptachKnifeInputActivity();
        }

        public void SetKnifes(int count)
        {
            Assert.AreNotEqual(count, settings.knifeObjects.Count, " the obects are not equal");
            ResetKnifes();
            for (int i = 0; i < count; i++)
            {
                settings.knifeObjects[i].SetActive(true);
            }
        }

        public void DeductKnifes()
        {
            if (settings.knifeObjects.Count <= 0)
                return;
            for (int i = 0; i < settings.knifeObjects.Count; i++)
            {
                if (settings.knifeObjects[i].activeSelf)
                {
                    settings.knifeObjects[i].SetActive(false);
                    break;
                }
            }

        }

        private void ResetKnifes()
        {
            for (int i = 0; i < settings.knifeObjects.Count; i++)
            {

                settings.knifeObjects[i].SetActive(false);


            }
        }

        private void UpdateScore(int score)
        {
            settings.txtScore.text = score.ToString();
        }

        private void UpdateStage(int stage)
        {
            settings.txtStage.text = stage.ToString();
            StartCoroutine(ShowStageObject(stage));
        }

        IEnumerator ShowStageObject(int stageId)
        {
            ScreenFader._instance.FadeScreen(FadeColor.Black, FadeType.FadeOut);
            settings.stageDisplayObj.SetActive(true);
            settings.txtStageNo.text = "STAGE "+stageId.ToString();
            yield return new WaitForSeconds(1f);
            settings.stageDisplayObj.SetActive(false);
        }

        [System.Serializable]
        public class Settings
        {
            public List<GameObject> knifeObjects;

            public TMP_Text txtScore;
            public TMP_Text txtStage;

            public GameObject stageDisplayObj;
            public TMP_Text txtStageNo;
        }
    }
}

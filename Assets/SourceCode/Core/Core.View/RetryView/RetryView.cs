﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Core.ScoreSystem;

namespace Core.Views
{
    public class RetryView : View<RetryView>
    {
        [SerializeField] private Settings settings;
        private IScoreManager scoreManager;

        public override void Awake()
        {
            scoreManager = FindObjectOfType<ScoreManager>();
            base.Awake();
        }
        public override void OnEnable()
        {
            InitView();
            settings.btnHome.onClick.AddListener(OnHomeClick);
            settings.btnRetry.onClick.AddListener(OnRetryGame);
            base.OnEnable();
        }

        public override void OnDisable()
        {
            settings.btnHome.onClick.RemoveListener(OnHomeClick);
            settings.btnRetry.onClick.RemoveListener(OnRetryGame);

            base.OnDisable();
        }

        private void OnRetryGame()
        {
            ScreenFader._instance.FadeScreen(FadeColor.Black, FadeType.FadeOut);
            Close();
            EventDispatcher.DispatchOnGameStart();

        }

        private void OnHomeClick()
        {
            ScreenFader._instance.FadeScreen(FadeColor.Black, FadeType.FadeOut);

            Close();
            GotToView<PlayView>();
        }

        public void InitView()
        {
            settings.txtScore.text = scoreManager.CurrentScore.ToString();
            settings.txtStage.text = scoreManager.CurrentStage.ToString();
            settings.txtHighScore.text = scoreManager.HighScore.ToString();
            settings.txtHighStage.text = scoreManager.HighStage.ToString();
        }

        [System.Serializable]
        public class Settings
        {
            public Button btnRetry;
            public Button btnHome;

            public TMP_Text txtStage;
            public TMP_Text txtScore;

            public TMP_Text txtHighStage;
            public TMP_Text txtHighScore;

        }
    }
}

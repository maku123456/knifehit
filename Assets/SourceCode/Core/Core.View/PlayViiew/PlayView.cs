﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Core.ScoreSystem;

namespace Core.Views
{
    public class PlayView : View<PlayView>
    {
        [SerializeField] public Settings settings;
        private IScoreManager scoreManager;

        private void Awake()
        {
            scoreManager = FindObjectOfType<ScoreManager>();
        }
       
        public override void OnEnable()
        {
            InitView();
            settings.btnPlay.onClick.AddListener(OnPlayClick);
            base.OnEnable();
        }

        public override void OnDisable()
        {
            settings.btnPlay.onClick.RemoveListener(OnPlayClick);
            base.OnDisable();
        }

        private void OnPlayClick()
        {
            Close();
            EventDispatcher.DispatchOnGameStart();
        }

        private void InitView()
        {
            settings.txtScore.text = scoreManager.HighScore.ToString();
            settings.txtStage.text = scoreManager.HighStage.ToString();
        }

        [System.Serializable]
        public class Settings
        {
            public Button btnPlay;
            public TMP_Text txtStage;
            public TMP_Text txtScore;
        }
    }
}
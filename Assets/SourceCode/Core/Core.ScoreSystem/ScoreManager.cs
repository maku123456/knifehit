﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.ScoreSystem
{
    public interface IScoreManager
    {
        int CurrentStage { get; set; }
        int CurrentScore { get; set; }
        int HighScore { get; set; }
        int HighStage { get; set; }
    }
    public sealed class ScoreManager : MonoBehaviour, IScoreManager
    {
        public int CurrentStage { get ; set ; }
        public int CurrentScore { get ; set ; }
        public int HighScore { get; set ; }
        public int HighStage { get ; set ; }

        private void Awake()
        {
            ResetScore();
            SetHighScores();
        }

        private void OnEnable()
        {
            EventDispatcher.OnGameStartedEvent += ResetScore;
            EventDispatcher.onBoardScoreUpdate += UpdateScore;
            EventDispatcher.onStageLoadEvent += UpdateStage;
        }

        private void OnDisable()
        {
            EventDispatcher.OnGameStartedEvent -= ResetScore;
            EventDispatcher.onBoardScoreUpdate -= UpdateScore;
        }

        private void SetHighScores()
        {
            HighScore = 0;
            HighStage = 1;
            if (PlayerPrefs.HasKey(GameConstant.SCOREPLAYER))
            {
                HighScore = PlayerPrefs.GetInt(GameConstant.SCOREPLAYER);
            }
            if (PlayerPrefs.HasKey(GameConstant.STAGEPLAYER))
            {
                HighStage = PlayerPrefs.GetInt(GameConstant.STAGEPLAYER);
            }
        }

        private void UpdateScore()
        {
            CurrentScore++;
            if(CurrentScore>HighScore)
            {
                HighScore = CurrentScore;
                PlayerPrefs.SetInt(GameConstant.SCOREPLAYER, HighScore);
            }
            EventDispatcher.DispatchOnScoreUpdate(CurrentScore);
        }

        private void UpdateStage(int stageId)
        {
            CurrentStage = stageId;
            if (CurrentStage >= HighStage)
            {
                HighStage = CurrentStage;
                PlayerPrefs.SetInt(GameConstant.STAGEPLAYER, HighStage);
            }
        }

        private void ResetScore()
        {
            CurrentStage = 0;
            CurrentScore = 0;
            EventDispatcher.DispatchOnScoreUpdate(CurrentScore);

        }
    }
}

﻿using Core.Concrete;
using Core.Impl;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.WorldStage
{
    public class WorldStageSnapShot : MonoBehaviour
    {
        [SerializeField] private TextAsset txtStageSnap;
        IWorldStageHandler worldStageHandler;
        private void Awake()
        {
            worldStageHandler = FindObjectOfType<WorldStageHandler>();
        }

        private void Start()
        {
            SnapShotCreator(txtStageSnap.text);
        }

        private void SnapShotCreator(string json)
        {
            var obj = JsonConvert.DeserializeObject<WorldStageModel>(json);
            worldStageHandler.SetUpWorldStage(obj);
        }


    }
}
﻿using Core.Concrete;
using Core.Impl;
using System.Collections;
using UnityEngine;

namespace Core.WorldStage
{
    public class WorldStageHandler : MonoBehaviour, IWorldStageHandler
    {
        public IWorldStage WorldStage { get; set; }
        public IStage CurrentStage { get; set; }

        public IStage GetNextStage()
        {
            var stage = WorldStage.GetStageById(CurrentStage.StageUnit.StageId + 1);
            CurrentStage = stage;
            return stage; ;
        }

        public IStage GetStartStage()
        {
            var stage = WorldStage.GetStageById(1);
            CurrentStage = stage;
            return stage; ;
        }

        public void SetUpWorldStage(WorldStageModel worldStage)
        {
            WorldStage = worldStage;
        }
    }
}
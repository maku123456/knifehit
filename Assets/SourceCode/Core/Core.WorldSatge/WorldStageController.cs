﻿using Core.Concrete;
using Core.Gameplay.Board;
using Core.Gameplay.Knife;
using Core.Impl;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.WorldStage
{
    public class WorldStageController : MonoBehaviour, IWorldStageController
    {
        public IKnifeController KnifeController { get; set; }
        public IWorldStageHandler WorldStageHandler { get; set; }
        public IBoard Board { get ; set ; }

        private void Start()
        {
            KnifeController = FindObjectOfType<KnifeController>();
            WorldStageHandler = FindObjectOfType<WorldStageHandler>();
            Board = FindObjectOfType<Board>();
        }

        private void OnEnable()
        {
            EventDispatcher.OnGameStartedEvent += StartStage;
            EventDispatcher.onLoadNextStageEvent += SetNextStage;
        }

        private void OnDisable()
        {
            EventDispatcher.OnGameStartedEvent -= StartStage;
            EventDispatcher.onLoadNextStageEvent -= SetNextStage;
        }

        private void SetNextStage()
        {
            var stage = WorldStageHandler.GetNextStage();
            Debug.Log("GetNextStage" + JsonConvert.SerializeObject(stage));
            KnifeController.SetUpKnifeStage(stage);
            Board.InitializeStage(stage);
        }

        public void StartStage()
        {
            var stage = WorldStageHandler.GetStartStage();
            Debug.Log("GetNextStage" + JsonConvert.SerializeObject(stage));
            KnifeController.SetUpKnifeStage(stage);
            Board.InitializeStage(stage);

        }
    }
}
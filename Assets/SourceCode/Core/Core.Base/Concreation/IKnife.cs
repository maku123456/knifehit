﻿using UnityEngine;

namespace Core.Concrete
{
    #region KNIFE ABSTRACTS

    public interface IKnife
    {
        Vector2 ThrowForce { get; }

        void InitKnife();

        void ThrowKnife();

        bool IsKnifeUseable { get; set; }
    }
}

#endregion
﻿using System.Collections.Generic;

namespace Core.Concrete
{
    #region WorldStages

    public interface IStage
    {
        IStageUnit StageUnit { get; set; }
        List<IRotationModel> RotationModel { get; set; }

    }
}

#endregion
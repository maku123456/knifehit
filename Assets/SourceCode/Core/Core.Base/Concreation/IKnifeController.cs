﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Concrete
{
    #region KNIFE ABSTRACTS

    public interface IKnifeController
    {
        IKnifeUnit TotalKnifeCount { get; set; }
        IKnifeUnit KnifeCount { get; set; }
        GameObject KnifeObject { get; }
        Vector2 SpawnPosition { get; }
        IStage CurrentStage { get; set; }

        IKnife CurrentKnife { get; set; }
        List<IKnife> KnifeStorage { get; set; }
        void SpawnKnife();
        void OnSuccesfullKnifeHit(Transform target);
        void OnUnSuccesfulKnifeHit(Transform target);

        void SetUpKnifeStage(IStage stage);
    }
}

#endregion
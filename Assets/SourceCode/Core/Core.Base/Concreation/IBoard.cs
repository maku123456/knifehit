﻿namespace Core.Concrete
{
    #region WorldStages

    #endregion

    #region BoardClass Abtraction

    public interface IBoard
    {
        IStage CurrentStage { get; set; }

        IBoardRotater BoardRotater { get; set; }

        void InitializeStage(IStage stage);
    }
}

#endregion
﻿using System.Collections.Generic;

namespace Core.Concrete
{
    #region WorldStages

    public interface IWorldStage
    {
        List<IStage> Stages { get; set; }

        IStage GetStageById(int stageId);
    }
}

#endregion
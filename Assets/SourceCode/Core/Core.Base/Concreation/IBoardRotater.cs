﻿using System.Collections;

namespace Core.Concrete
{
    public interface IBoardRotater
    {
        IStage CurrentStage { get; set; }

        bool ResetBoardRoattion { get; set; }

        void Rotate(int rotationIndex);
    }
}


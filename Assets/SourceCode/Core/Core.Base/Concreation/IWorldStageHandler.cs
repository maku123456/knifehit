﻿using Core.Impl;

namespace Core.Concrete
{
    #region WorldStages


    public interface IWorldStageHandler
    {
        IWorldStage WorldStage { get; set; }

        IStage CurrentStage { get; set; }

        IStage GetNextStage();

        IStage GetStartStage();

        void SetUpWorldStage(WorldStageModel worldStage);
    }
}

#endregion
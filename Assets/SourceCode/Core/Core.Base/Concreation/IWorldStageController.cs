﻿namespace Core.Concrete
{
    #region WorldStages

    public interface IWorldStageController
    {
        IKnifeController KnifeController { get; set; }

        IWorldStageHandler WorldStageHandler { get; set; }

        IBoard Board { get; set; }

        void StartStage();
    }
}

#endregion
﻿namespace Core.Concrete
{
    #region WorldStages
    public interface IRotationModel
    {
        float Speed { get; set; }
        float Duration { get; set; }
    }
}

#endregion
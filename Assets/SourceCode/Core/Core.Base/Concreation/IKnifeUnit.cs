﻿namespace Core.Concrete
{
    #region KNIFE ABSTRACTS
    public interface IKnifeUnit
    {
        int Count { get; set; }
    }
}

#endregion
﻿namespace Core.Concrete
{
    #region WorldStages

    public interface IStageUnit : IKnifeUnit
    {
        int StageId { get; set; }
    }
}

#endregion
﻿using Core.Concrete;

namespace Core.Impl
{
    public class StageUnit : IStageUnit
    {
        public int StageId { get; set; }
        public int Count { get; set; }
    }
}
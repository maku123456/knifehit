﻿using Core.Concrete;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Impl
{
    public class WorldStageModel : IWorldStage
    {
        [JsonConverter(typeof(ConcreteTypeListConverter<IStage, Stage>))]
        public List<IStage> Stages { get; set; }

        public IStage GetStageById(int stageId)
        {
            for (int i = 0; i < Stages.Count; i++)
            {
                if (Stages[i].StageUnit.StageId == stageId)
                    return Stages[i];
            }
            return null;
        }
    }
}
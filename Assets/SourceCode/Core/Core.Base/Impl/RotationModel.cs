﻿using Core.Concrete;

namespace Core.Impl
{
    public class RotationModel : IRotationModel
    {
        public float Speed { get; set; }
        public float Duration { get; set; }
    }
}
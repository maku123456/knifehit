﻿using Core.Concrete;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Impl
{
    public class Stage : IStage
    {
        [JsonConverter(typeof(ConcreteConverter<StageUnit>))]
        public IStageUnit StageUnit { get; set; }
        [JsonConverter(typeof(ConcreteTypeListConverter<IRotationModel, RotationModel>))]
        public List<IRotationModel> RotationModel { get; set; }

    }
}
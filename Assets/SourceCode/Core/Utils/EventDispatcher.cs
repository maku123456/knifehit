﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventDispatcher
{
    public  delegate void OnnDispatch();
    public static event OnnDispatch onDispatchEvent;

    public delegate void OnKnifeInputActivity();
    public static event OnKnifeInputActivity onKnifeInputActivityEvent;

    public delegate void OnGameStarted();
    public static event OnGameStarted OnGameStartedEvent;

    public delegate void OnGameEnded();
    public static event OnGameEnded OnGameEndedEvent;

    public delegate void OnBoardHit(Transform target);
    public static event OnBoardHit onBoardHitEvent;

    public delegate void OnScoreUpdaOnBoardScoreUpdate();
    public static event OnScoreUpdaOnBoardScoreUpdate onBoardScoreUpdate;

    public delegate void OnKnifeHit(Transform target);
    public static event OnKnifeHit OnKnifeHitEvent;

    public delegate void OnLoadNextStage();
    public static event OnLoadNextStage onLoadNextStageEvent;
    
    public delegate void OnStageLoad(int stageId);
    public static event OnStageLoad onStageLoadEvent;

    public delegate void OnScoreUpdate(int score);
    public static event OnScoreUpdate onScoreUpdateEvent;

    public static void DispatchEvent()
    {
        onDispatchEvent?.Invoke();
    }

    public static void DisptachKnifeInputActivity()
    {
        onKnifeInputActivityEvent?.Invoke();
    }

    public static void DispatchOnGameStart()
    {
        OnGameStartedEvent?.Invoke();
    }

    public static void DispatchOnGameEnded()
    {
        OnGameEndedEvent?.Invoke();
    }

    public static void DisptachOnBoardHit(Transform target)
    {
        onBoardHitEvent?.Invoke(target);
        onBoardScoreUpdate?.Invoke();
    }
    public static void DisptachOnKnifeHit(Transform target)
    {
        OnKnifeHitEvent?.Invoke(target);
    }

    public static void DispatchOnLoadNextStage()
    {
        onLoadNextStageEvent?.Invoke();
    }

    public static void DisptachNextStage(int stageId)
    {
        onStageLoadEvent?.Invoke(stageId);
    }

    public static void DispatchOnScoreUpdate(int score)
    {
        onScoreUpdateEvent?.Invoke(score);
    }
}

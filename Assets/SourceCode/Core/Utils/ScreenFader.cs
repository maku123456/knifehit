﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FadeColor
{
    White,
    Black
}

public enum FadeType
{
    FadeIn,
    FadeOut
}

public class ScreenFader : MonoBehaviour
{

    [SerializeField]
    private Image img;

    [SerializeField]
    private FadeColor fadeColor;

    [SerializeField]
    private FadeType fadeType;

    [SerializeField]
    private float duration = 2;

    public static ScreenFader _instance = null;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void FadeScreen(FadeColor _fadeColor, FadeType _fadeType)
    {
        fadeType = _fadeType;
        fadeColor = _fadeColor;
        SetImageColor(fadeColor);
        if (fadeType == FadeType.FadeIn)
            FadeIn();
        else
            FadeOut();
    }

    private void FadeIn()
    {
        img.canvasRenderer.SetAlpha(0.0f);
        img.CrossFadeAlpha(1, duration, false);
    }

    private void FadeOut()
    {
        img.canvasRenderer.SetAlpha(1.0f);
        img.CrossFadeAlpha(0, duration, false);
    }

    private void SetImageColor(FadeColor _fadeColor)
    {
        if (_fadeColor == FadeColor.Black)
            img.color = Color.black;
        else
            img.color = Color.white;
    }

}

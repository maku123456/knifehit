﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Audio
{
    /// <summary>
    ///  This is a temporary class for playing  sound 
    /// </summary>
    public class AudioManager : MonoBehaviour
    {

        //TODO: fix this class add frame
        [SerializeField] AudioSource sourceBoard;
        [SerializeField] AudioSource sourceKnife;

        private void OnEnable()
        {
            EventDispatcher.onBoardHitEvent += PlayBoardHit;
            EventDispatcher.OnKnifeHitEvent += PlayKnifeHit;
        }

        private void OnDisable()
        {
            EventDispatcher.onBoardHitEvent -= PlayBoardHit;
            EventDispatcher.OnKnifeHitEvent -= PlayKnifeHit;

        }

        public void PlayBoardHit(Transform target)
        {
            sourceBoard.Play();
        }

        public void PlayKnifeHit(Transform target)
        {
            sourceKnife.Play();
        }
    }
}